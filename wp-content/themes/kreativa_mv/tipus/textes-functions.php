<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//
// New Post Type
//


add_action('init', 'mv_textes_register');  

function mv_textes_register() {
    $args = array(
        'label' => __('Textes_altres', 'mv_backend'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => true,
        'supports' => array('title','thumbnail','editor')
       );  

    register_post_type( 'textes' , $args );
}


//
// Thumbnail column
//



//
// Testimonial Title and Caption
//

add_action("admin_init", "mv_textes_title_settings");   

function mv_textes_title_settings(){
 //  echo "sssssssWWW";die();
    add_meta_box("textes_title_settings", "Textes", "mv_textes_title_config", "textes", "normal", "high");
}   

function mv_textes_title_config(){
        global $post;
        
       if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
        $custom = get_post_custom($post->ID);
		
		if(isset($custom["textes_content"][0])) $testimonial_content = $custom["textes_content"][0];
		if(isset($custom["role"][0])) $role = $custom["role"][0];
		if(isset($custom["name"][0])) $name = $custom["name"][0];
?>
	<div class="metabox-options form-table fullwidth-metabox image-upload-dep">
		
		<div class="metabox-option">
			<h6><?php _e('Name', 'mv_backend') ?>:</h6>
			<input type="text" name="name" value="<?php echo $name; ?>">
		</div>		
		
		<div class="metabox-option">
			<h6><?php _e('Role', 'mv_backend') ?>:</h6>
			<input type="text" name="role" value="<?php echo $role; ?>">
		</div>
		
		<div class="metabox-option">
			<h6><?php _e('Textes', 'mv_backend') ?>:</h6>
			<textarea name="textes_content"><?php echo $testimonial_content; ?></textarea>
		</div>
		
	</div>
<?php
    }	
	
	
// Save Slide
	
add_action('save_post', 'mv_save_textes_meta'); 

function mv_save_textes_meta(){
    global $post;  	
	
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{
	
		$post_metas = array('name','role','textes_content');
		
		foreach($post_metas as $post_meta) {
			if(isset($_POST[$post_meta])) update_post_meta($post->ID, $post_meta, $_POST[$post_meta]);
		}
    }

}
